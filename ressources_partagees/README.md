# Hackathon "Données du Grand Débat" le 23 Mars 2019 : suivi de la journée 

## Nom des groupes 

- 1/ Grande Annotation : [site web](https://grandeannotation.fr/) + [data](https://framagit.org/fm89/hack-gdn)
- 2/ Analyse comparative des contributions citoyennes : [source](https://framagit.org/someonefefe/datathon-grand-debat/tree/master/Temporal%20Analysis) et [slides](https://framagit.org/parlement-ouvert/datathon-grand-debat/blob/master/ressources_partagees/slides/Temporal_Analysis_HackGDN.pdf)
- 3/ Que pensent mes voisins ?: [slides](https://framagit.org/parlement-ouvert/datathon-grand-debat/blob/master/ressources_partagees/slides/Que_pensent_mes_voisins_hackGDN.pdf)
- 4 / Analyse des contributions Lille : [source](https://github.com/OpenSourcePolitics/datathon-grand-debat) + slides ?
- 5/ Ensemble : [slides](https://framagit.org/parlement-ouvert/datathon-grand-debat/blob/master/ressources_partagees/slides/HackGDN_Ensemble_la_france.pdf) 
- 6/ Entendre la France: [slides](https://framagit.org/parlement-ouvert/datathon-grand-debat/blob/master/ressources_partagees/slides/Hackathon_ELF.pdf)
- 7/ Code For France :
- 8/ La Grande lecture : [source](https://github.com/cquest/grande_lecture)
- 9/ Les grandes idées (outil parlementaire) : [source](https://framagit.org/matDou/datathon-grand-debat) + [slides](https://docs.google.com/presentation/d/1-i79v9lzOEpgL-VJPZnSmE8HQ5LGWBFM-ekKyJWuXK0/edit#slide=id.g5509f4954e_4_0)
- 10/ Démocratie.app: [source](https://github.com/gauthier-schweitzer/grand_debat_hackathon) + [slides](https://framagit.org/parlement-ouvert/datathon-grand-debat/blob/master/ressources_partagees/slides/Democratie_app_hackGDN.pptx) + [site web](https://democratie.app/)
- 11/ Circonscription 360 : ? 
- 12 / Analyse des données du grand débat : ? 
- 13 / QCM : ?
- 14/ Visualisation de réponses aux questions ouvertes :[notebook](https://framagit.org/matDou/datathon-grand-debat/blob/master/visualisation.ipynb) et [slides](https://framagit.org/parlement-ouvert/datathon-grand-debat/tree/master/ressources_partagees/slides/Visualisation)


---

Non présentés : 
- 15/ Données vrai débat : https://github.com/c24b/vrai-debat
- 16/ Navigateur du GDN : https://github.com/ValentinRicher/nav-grand-debat
- 17/ Cours des comptes : ?




### Organisation intergroupes 

- Discussion intergroupes : https://chat.codefor.fr/ (channel #opendebat)
- Mise en ligne des slides fin de journée : https://framagit.org/parlement-ouvert/datathon-grand-debat/tree/master/ressources_partagees/slides

### Dataset utilisés 

- Grand Débat Plateforme : https://granddebat.fr/pages/donnees-ouvertes
- Grande Annotation : https://grandeannotation.fr/data
- Dossier compilant les données de la plateforme + Entendre la France + Grande Annotation accessible [ici](https://drive.google.com/drive/folders/1f49KQl8p9zL4FUMn2q2__tBL8oWyCkhI)
- Vrai debat : plateforme alternative au Grand Débat National données des propositions par theme disponible dans ce  [dossier](https://cloud.driss.org/index.php/s/muN3N9JzcSRWKpp) Mot de passe: `Parlement-Ouvert` 
- Dataset GDN Lille : https://github.com/OpenSourcePolitics/datathon-grand-debat/tree/master/data  
- INSEE : 

### Autres 

- Hashtag Twitter #HackGDN https://twitter.com/hashtag/hackGDN?src=hash
- Entendre la France vos réponses : https://reponses.entendrelafrance.fr/












