# Explication graphe "entre"

Le pic en fin de période du GDN correspond a la surrepresentation du message
suivant:


"Supprimer la CSG pour TOUS les retraités. Supprimer la Taxe d'habitation pour TOUS les retraités. Les retraités entre 2000 et 2500€ bruts sont les grands perdants car, après imposition, il leur reste moins que ceux qui gagnent un peu moins et bénéficient des mesures gouvernementales." 