import urllib.request
import requests # charge page et stocke dans une var
from bs4 import BeautifulSoup# librairie permettant de ne voir que le html d'une page
import csv
import re

##### VAR
outfile="/Users/mathildelonguet/Documents/grand_debat/twitter_output.csv"  # attention le dossier doit deja etre existant
infile="/Users/mathildelonguet/Documents/grand_debat/comptestwittergdn.csv"
#listUrl = ["https://twitter.com/michtosincere", "https://twitter.com/Le_Stagirite"]

##### READ url
with open(infile,"r") as file:
    reader=csv.reader(file,delimiter='\n')
    next(reader)
    listUrl=list(reader)

##### SCRAP
with open(outfile, "w", encoding='utf-8') as csv_file:
    writer = csv.writer(csv_file, delimiter=';')
    for url in listUrl :
        page = requests.get(url[0]).content
        soup = BeautifulSoup(page)
        regex =  re.search(r'http://twitter.com/(.*)',url[0])
        for tag in soup.find_all("p", class_="ProfileHeaderCard-bio u-dir") : #find_all fait une liste donc  a parcourir
            #writer.writerow({regex.group(1),tag.text})
            bio=tag.text.replace('\n', ' ')
            print(regex.group(1)+bio)
            print(regex.group(1)+";"+bio, file=csv_file)
