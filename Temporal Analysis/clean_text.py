"""
in console : pip install nltk


then first in a cell run: (to download nltk stopwords)

import nltk
nltk.download('stopwords')

"""

import re
from nltk.corpus import stopwords
from nltk.stem.snowball import FrenchStemmer


FRENCH_STOPWORDS = set(stopwords.words('french'))
FRENCH_STOPWORDS.update({'les', 'a'})
STEMMER = FrenchStemmer()


def remove_stopwords(text):
    return " ".join([word for word in text.split() if word not in FRENCH_STOPWORDS])


def remove_specials(text):
    return re.sub("[-,\''’:.()|]", " ", text)


def stem(tokens):
    return [STEMMER.stem(token) for token in tokens]


clean_callbacks = [
    str.lower,  # lowercase
    remove_specials,  # remove few special characters
    remove_stopwords,  # remove unnecessary words
    str.split,  # tokenize
    stem  # apply stemming
]


def clean(text):
    for _callback in clean_callbacks:
        text = _callback(text)
    return text
